use super::aabb::Aabb;
use super::flip_normals::FlipNormals;
use super::hitable::{HitRecord, Hitable};
use super::hitable_list::HitableList;
use super::material::Material;
use super::ray::Ray;
use super::vec3::Vec3;
use super::xy_rect::XYRect;
use super::xz_rect::XZRect;
use super::yz_rect::YZRect;
use std::sync::Arc;

pub struct CBox {
  pub pmin: Vec3,
  pub pmax: Vec3,
  pub list: Arc<Hitable>,
}

impl CBox {
  pub fn new(p0: Vec3, p1: Vec3, mtl: Arc<Material>) -> CBox {
    let mut objs = HitableList::new();

    objs.add_object(Arc::new(XYRect::new(
      p0.x,
      p1.x,
      p0.y,
      p1.y,
      p1.z,
      mtl.clone(),
    )));

    objs.add_object(Arc::new(FlipNormals::new(Arc::new(XYRect::new(
      p0.x,
      p1.x,
      p0.y,
      p1.y,
      p0.z,
      mtl.clone(),
    )))));

    objs.add_object(Arc::new(XZRect::new(
      p0.x,
      p1.x,
      p0.z,
      p1.z,
      p1.y,
      mtl.clone(),
    )));

    objs.add_object(Arc::new(FlipNormals::new(Arc::new(XZRect::new(
      p0.x,
      p1.x,
      p0.z,
      p1.z,
      p0.y,
      mtl.clone(),
    )))));

    objs.add_object(Arc::new(YZRect::new(
      p0.y,
      p1.y,
      p0.z,
      p1.z,
      p1.x,
      mtl.clone(),
    )));

    objs.add_object(Arc::new(FlipNormals::new(Arc::new(YZRect::new(
      p0.y,
      p1.y,
      p0.z,
      p1.z,
      p0.x,
      mtl.clone(),
    )))));

    CBox {
      pmin: p0,
      pmax: p1,
      list: Arc::new(objs),
    }
  }
}

impl Hitable for CBox {
  fn hit(&self, r: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
    self.list.hit(r, t_min, t_max)
  }

  fn bounding_box(&self, _t0: f32, _t1: f32) -> Option<Aabb> {
    Some(Aabb::new(self.pmin, self.pmax))
  }
}
