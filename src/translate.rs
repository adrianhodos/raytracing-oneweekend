use super::aabb::Aabb;
use super::hitable::{HitRecord, Hitable};
use super::ray::Ray;
use super::vec3::Vec3;
use std::sync::Arc;

pub struct Translate {
  obj: Arc<Hitable>,
  offset: Vec3,
}

impl Translate {
  pub fn new(obj: Arc<Hitable>, displacement: Vec3) -> Translate {
    Translate {
      obj,
      offset: displacement,
    }
  }
}

impl Hitable for Translate {
  fn hit(&self, r: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
    let moved_r = Ray::new(r.origin - self.offset, r.direction, r.time);
    if let Some(mut hitrec) = self.obj.hit(&moved_r, t_min, t_max) {
      hitrec.p += self.offset;
      Some(hitrec)
    } else {
      None
    }
  }

  fn bounding_box(&self, t0: f32, t1: f32) -> Option<Aabb> {
    if let Some(bbox) = self.obj.bounding_box(t0, t1) {
      Some(Aabb::new(bbox.min + self.offset, bbox.max + self.offset))
    } else {
      None
    }
  }
}
