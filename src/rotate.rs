use super::aabb::Aabb;
use super::hitable::{HitRecord, Hitable};
use super::ray::Ray;
use super::vec3::Vec3;
use std::sync::Arc;

pub struct RotateY {
  obj: Arc<Hitable>,
  sin_theta: f32,
  cos_theta: f32,
  bbox: Aabb,
}

impl RotateY {
  pub fn new(obj: Arc<Hitable>, angle: f32) -> RotateY {
    let radians = (std::f32::consts::PI / 180_f32) * angle;
    let sin_theta = radians.sin();
    let cos_theta = radians.cos();
    let bbox = obj.bounding_box(0_f32, 1_f32).unwrap_or_default();

    let mut min = Vec3::same(std::f32::MAX);
    let mut max = Vec3::same(std::f32::MIN);

    for i in 0..2 {
      for j in 0..2 {
        for k in 0..2 {
          let x = i as f32 * bbox.max.x + (1_f32 - i as f32) * bbox.min.x;
          let y = j as f32 * bbox.max.y + (1_f32 - j as f32) * bbox.min.y;
          let z = k as f32 * bbox.max.z + (1_f32 - k as f32) * bbox.min.z;

          let tester = Vec3::new(
            cos_theta * x + sin_theta * z,
            y,
            -sin_theta * x + cos_theta * z,
          );

          for c in 0..3 {
            if tester[c as usize] > max[c as usize] {
              max[c as usize] = tester[c as usize];
            }

            if tester[c as usize] < min[c as usize] {
              min[c as usize] = tester[c as usize];
            }
          }
        }
      }
    }

    RotateY {
      obj,
      sin_theta,
      cos_theta,
      bbox: Aabb::new(min, max),
    }
  }
}

impl Hitable for RotateY {
  fn hit(&self, r: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
    let mut origin = r.origin;
    let mut direction = r.direction;

    origin[0] = self.cos_theta * r.origin[0] - self.sin_theta * r.origin[2];
    origin[2] = self.sin_theta * r.origin[0] + self.cos_theta * r.origin[2];

    direction[0] =
      self.cos_theta * r.direction[0] - self.sin_theta * r.direction[2];
    direction[2] =
      self.sin_theta * r.direction[0] + self.cos_theta * r.direction[2];

    let rotated_r = Ray::new(origin, direction, r.time);

    if let Some(mut hitrec) = self.obj.hit(&rotated_r, t_min, t_max) {
      let mut p = hitrec.p;
      let mut n = hitrec.normal;

      p[0] = self.cos_theta * hitrec.p[0] + self.sin_theta * hitrec.p[2];
      p[2] = -self.sin_theta * hitrec.p[0] + self.cos_theta * hitrec.p[2];

      n[0] =
        self.cos_theta * hitrec.normal[0] + self.sin_theta * hitrec.normal[2];
      n[2] =
        -self.sin_theta * hitrec.normal[0] + self.cos_theta * hitrec.normal[2];

      hitrec.p = p;
      hitrec.normal = n;

      Some(hitrec)
    } else {
      None
    }
  }

  fn bounding_box(&self, t0: f32, t1: f32) -> Option<Aabb> {
    Some(self.bbox)
  }
}
