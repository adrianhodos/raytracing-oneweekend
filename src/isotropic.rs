use super::hitable::{HitRecord, Hitable};
use super::material::Material;
use super::ray::Ray;
use super::texture::Texture;
use super::vec3::{random_in_unit_sphere, Vec3};
use std::sync::Arc;

pub struct Isotropic {
  albedo: Arc<Texture>,
}

impl Isotropic {
  pub fn new(tex: Arc<Texture>) -> Isotropic {
    Isotropic { albedo: tex }
  }
}

impl Material for Isotropic {
  fn scatter(&self, _r: &Ray, h: &HitRecord) -> Option<(Vec3, Ray)> {
    Some((
      self.albedo.value(h.u, h.v, h.p),
      Ray::new(h.p, random_in_unit_sphere(), 0_f32),
    ))
  }
}
