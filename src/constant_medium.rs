use super::aabb::Aabb;
use super::hitable::{HitRecord, Hitable};
use super::isotropic::Isotropic;
use super::material::Material;
use super::ray::Ray;
use super::texture::Texture;
use super::vec3::Vec3;
use rand::prelude::*;
use std::sync::Arc;

pub struct ConstantMedium {
  boundary: Arc<Hitable>,
  phase_function: Arc<Material>,
  density: f32,
}

impl ConstantMedium {
  pub fn new(b: Arc<Hitable>, d: f32, a: Arc<Texture>) -> ConstantMedium {
    ConstantMedium {
      boundary: b,
      phase_function: Arc::new(Isotropic::new(a)),
      density: d,
    }
  }
}

impl Hitable for ConstantMedium {
  fn hit(&self, r: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
    if let Some(mut rec1) = self.boundary.hit(r, std::f32::MIN, std::f32::MAX) {
      if let Some(mut rec2) =
        self.boundary.hit(r, rec1.t + 0.0001_f32, std::f32::MAX)
      {
        if rec1.t < t_min {
          rec1.t = t_min;
        }

        if rec2.t > t_max {
          rec2.t = t_max;
        }

        if rec1.t >= rec2.t {
          return None;
        }

        if rec1.t < 0_f32 {
          rec1.t = 0_f32;
        }

        let distance_inside_boundary = (rec2.t - rec1.t) * r.direction.length();
        let hit_distance =
          -(1_f32 / self.density) * thread_rng().gen::<f32>().ln();

        if hit_distance < distance_inside_boundary {
          let t = rec1.t + hit_distance / r.direction.length();
          let rec = HitRecord::new(
            t,
            r.point_at_param(t),
            Vec3::new(1_f32, 0_f32, 0_f32),
            self.phase_function.clone(),
            rec1.u,
            rec1.v,
          );

          return Some(rec);
        }
      }
    }

    None
  }

  fn bounding_box(&self, t0: f32, t1: f32) -> Option<Aabb> {
    self.boundary.bounding_box(t0, t1)
  }
}
